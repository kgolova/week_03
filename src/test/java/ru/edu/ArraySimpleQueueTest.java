package ru.edu;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class ArraySimpleQueueTest extends TestCase {

    public static final String elem1 = "elem1";
    public static final String elem2 = "elem2";
    public static final String elem3 = "elem3";
    public static final String elem4 = "elem4";

    private ArraySimpleQueue<String> queue;

    @Before
    public void setUp() {
        queue = new ArraySimpleQueue<>(5);
        queue.offer(elem1);
        queue.offer(elem2);
        queue.offer(elem3);
    }

    @Test
    public void testOffer() {
        queue.offer(elem4);
        assertEquals(4, queue.size());
    }

    @Test
    public void testOfferIsFull() {
        queue.offer(elem4);
        queue.offer("elem5");

        boolean success = false;

        try {
            queue.offer("6");
        } catch (IllegalArgumentException e) {
            success = true;
        }

        assertTrue(success);
    }

    @Test
    public void testPoll() {
        queue.poll();
        assertEquals(2, queue.size());
    }

    @Test
    public void testPollIllegalArgumentException() {
        ArraySimpleQueue queueTest = new ArraySimpleQueue<>(5);

        boolean success = false;

        try {
            queueTest.poll();
        } catch (IllegalArgumentException e) {
            success = true;
        }

        assertTrue(success);
    }

    @Test
    public void testPeek() {
        assertEquals(elem1, queue.peek());
    }

    @Test
    public void testPeekIllegalArgumentException() {
        ArraySimpleQueue queueTest = new ArraySimpleQueue<>(5);

        boolean success = false;

        try {
            queueTest.peek();
        } catch (IllegalArgumentException e) {
            success = true;
        }

        assertTrue(success);
    }

    @Test
    public void testSize() {
        assertEquals(3, queue.size());
    }

    @Test
    public void testCapacity() {
        assertEquals(2, queue.capacity());
    }
}
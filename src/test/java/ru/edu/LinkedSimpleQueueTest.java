package ru.edu;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class LinkedSimpleQueueTest extends TestCase {

    public static final String elem1 = "elem1";
    public static final String elem2 = "elem2";
    public static final String elem3 = "elem3";
    public static final String elem4 = "elem4";

    private LinkedSimpleQueue<String> list;

    @Before
    public void setUp() {
        list = new LinkedSimpleQueue<>(5);
        list.offer(elem1);
        list.offer(elem2);
        list.offer(elem3);
    }

    @Test
    public void testOffer() {
        list.offer(elem4);
        assertEquals(4, list.size());
    }

    @Test
    public void testOfferIndexOutOfBoundsException() {
        LinkedSimpleQueue<String> listTest = new LinkedSimpleQueue<>(2);
        listTest.offer(elem1);
        listTest.offer(elem2);
        boolean success = false;
        try {
            listTest.offer(elem3);
        } catch (IndexOutOfBoundsException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testPoll() {
        assertEquals(elem1, list.poll());
        assertEquals(2, list.size());
    }

    @Test
    public void testPollIllegalArgumentException() {
        LinkedSimpleQueue<String> listTest = new LinkedSimpleQueue<>(2);
        boolean success = false;
        try {
            listTest.poll();
        } catch (IllegalArgumentException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testPeek() {
        assertEquals(elem1, list.peek());
    }

    @Test
    public void testPeekIllegalArgumentException() {
        LinkedSimpleQueue<String> listTest = new LinkedSimpleQueue<>(2);
        boolean success = false;
        try {
            listTest.peek();
        } catch (IllegalArgumentException e) {
            success = true;
        }
        assertTrue(success);
    }


    @Test
    public void testSize() {
        assertEquals(3, list.size());
    }

    @Test
    public void testCapacity() {
        assertEquals(2, list.capacity());
    }
}
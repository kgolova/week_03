package ru.edu;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class LinkedSimpleListTest extends TestCase {

    public static final String elem1 = "elem1";
    public static final String elem2 = "elem2";
    public static final String elem3 = "elem3";
    public static final String elem4 = "elem4";

    private SimpleList<String> list;


    @Before
    public void setUp() throws Exception {
        list = new LinkedSimpleList<>();
        list.add(elem1);
        list.add(elem2);
        list.add(elem3);
    }

    @Test
    public void testAdd() {
        list.add(elem4);
        assertEquals(elem4, list.get(3));
    }

    @Test
    public void testSet() {
        list.set(2, elem4);
        assertEquals(elem4, list.get(2));
    }

    @Test
    public void testSetIndexOutOfBoundsException() {
        boolean success = false;
        try {
            list.set(8, elem4);
        } catch (IndexOutOfBoundsException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testGet() {
        assertEquals(elem1, list.get(0));
    }

    @Test
    public void testGetIndexOutOfBoundsException() {
        boolean success = false;
        try {
            list.get(8);
        } catch (IndexOutOfBoundsException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testRemove() {
        assertEquals(3, list.size());
        list.remove(2);
        assertEquals(2, list.size());
    }

    @Test
    public void testRemoveIndexOutOfBoundsException() {
        boolean success = false;
        try {
            list.remove(8);
        } catch (IndexOutOfBoundsException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testIndexOf() {
        assertEquals(0,  list.indexOf(elem1));
        assertEquals(-1, list.indexOf(elem4));
    }

    @Test
    public void testSize() {
        assertEquals(3, list.size());
    }
}
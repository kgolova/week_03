package ru.edu;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class ArraySimpleListTest extends TestCase {

    public static final String elem1 = "elem1";
    public static final String elem2 = "elem2";
    public static final String elem3 = "elem3";
    public static final String elem4 = "elem4";

    private ArraySimpleList<String> list;

    @Before
    public void setUp() {

        list = new ArraySimpleList<>(4);
        list.add(elem1);
        list.add(elem2);
        list.add(elem3);
    }

    @Test
    public void testAdd() {
        list.add(elem2);
        assertEquals(elem2, list.get(1));
    }

    @Test
    public void testSet() {
        list.set(2, "elem3");
        assertEquals("elem3", list.get(2));
    }

    @Test
    public void testSetIndexOutOfBounds() {
        boolean success = false;

        try {
            list.set(8, "elem4");
        } catch (IndexOutOfBoundsException e) {
            success = true;
        }

        assertTrue(success);
    }

    @Test
    public void testGet() {
        assertEquals(elem1, list.get(0));
    }

    @Test
    public void testGetNull() {
        assertEquals(null, list.get(8));
    }

    @Test
    public void testRemove() {
        System.out.println(list.size());
        list.remove(2);
        assertEquals(2, list.size());
    }

    @Test
    public void testRemoveIndexOutOfBounds() {
        boolean success = false;

        try {
            list.remove(8);
        } catch (IndexOutOfBoundsException e) {
            success = true;
        }

        assertTrue(success);
    }

    @Test
    public void testIndexOf() {
        assertEquals(0,  list.indexOf(elem1));
        assertEquals(-1, list.indexOf(elem4));
    }

    public void testSize() {
        assertEquals(3, list.size());
    }
}
package ru.edu;

public class ArraySimpleQueue<T> implements SimpleQueue<T> {

    /**
     * Массив.
     */
    private T[] arr;

    /**
     * Количество элементов в очереди.
     */
    private int size = 0;

    /**
     * Размер очереди.
     */
    private int capacity = 0;

    /**
     * @param capacityParam - размер очереди.
     */
    public ArraySimpleQueue(final int capacityParam) {

        this.arr = (T[]) new Object[capacityParam];
        this.capacity = capacityParam;
    }


    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {

        if (size == capacity) {
            throw new IllegalArgumentException("Queue is full");
        }

        if (size + 1 > arr.length) {
            return false;
        }
        arr[size] = value;

        ++size;

        return true;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {

        T value;

        if (size == 0) {
            throw new IllegalArgumentException("Queue is empty");
        }

        value = arr[0];

        T[] old = arr;

        this.arr = (T[]) new Object[capacity];
        this.size = 0;

        for (int i = 1; i < old.length; i++) {
            this.arr[i - 1] = old[i];
            if (arr[i - 1] != null) {
                ++size;
            }

        }

        return value;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {

        if (size == 0) {
            throw new IllegalArgumentException("empty queue");
        }

        return arr[0];
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return capacity - size;
    }
}
